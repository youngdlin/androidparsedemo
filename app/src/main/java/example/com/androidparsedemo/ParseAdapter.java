package example.com.androidparsedemo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.util.List;

/**
 * Created by aneeshjindal on 3/3/16.
 */
public class ParseAdapter extends RecyclerView.Adapter<ParseAdapter.CustomViewHolder> {

    private Context context;
    private List<ParseObject> list;

    public ParseAdapter(Context context, List<ParseObject> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_view, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, int position) {
        ParseObject parseObject = list.get(position);

        ParseFile parseFile = (ParseFile) parseObject.get("image");
        parseFile.getDataInBackground(new GetDataCallback() {
            @Override
            public void done(byte[] data, ParseException e) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                holder.image.setImageBitmap(bitmap);
            }
        });
        holder.name.setText(parseObject.getString("name"));
        holder.description.setText(parseObject.getString("description"));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(List<ParseObject> list) {
        this.list = list;
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name;
        TextView description;

        public CustomViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            name = (TextView) itemView.findViewById(R.id.name);
            description = (TextView) itemView.findViewById(R.id.description);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainActivity.createUpdateDialog(name.getText().toString(), description.getText().toString());
                    Log.i("onClick", getAdapterPosition() + "");
                }
            });
        }
    }

}
